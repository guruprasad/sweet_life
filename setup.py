import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

requires = [
    'django==1.8',
    ]

setup(name='sweet_life',
      version='0.1',
      description='A logging application for diabetics written in Python/Django',
      long_description='An logging application for diabetics written in Python/Django',
      license = 'AGPLv3+',
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Django",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='L. Guruprasad',
      author_email='lgp171188@gmail.com',
      url='https://github.com/lgp171188/sweet_life/',
      keywords='web wsgi django diabetes',
      packages=find_packages(),
      install_requires=requires,
      )
